package br.com.observer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.com.observer.interfaces.Notifier;
import br.com.observer.listeners.SaleListener;
import br.com.observer.listeners.TruckListener;
import br.com.observer.model.Pedido;
import br.com.observer.notifiers.NotifierNovosPedidos;
import br.com.observer.utils.XmlReader;


public class Principal {	

	public static void main(String args[]) throws Exception{	
		File pedidosFolder = new File("C:\\Workspace\\Estudo\\PatternsAlgaWorks\\projetos-desafios-patterns\\DescarregadorPedidos");
		List<Pedido> pedidos = new ArrayList<Pedido>();	
		Notifier notifier = new NotifierNovosPedidos();
		notifier.register(new TruckListener(notifier));
		notifier.register(new SaleListener(notifier));
		
		while(true){
			File[] listOfFiles = pedidosFolder.listFiles();
			if(listOfFiles != null && listOfFiles.length>0){
				for (File file : listOfFiles) {
					pedidos.addAll(new XmlReader().readPedidos(file.getAbsolutePath()));
					notifier.novosPedidos(pedidos);
					notifier.notifi();
					file.delete();
				}	
			}else{
				Thread.sleep(5000);	
			}		
		}		
	}
}
