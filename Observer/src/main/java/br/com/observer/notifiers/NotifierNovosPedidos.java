package br.com.observer.notifiers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.observer.interfaces.Listener;
import br.com.observer.interfaces.Notifier;
import br.com.observer.model.Pedido;

public class NotifierNovosPedidos implements Notifier{
	private Set<Listener> listeners;
	private List<Pedido> novosPedidos;
	

	public NotifierNovosPedidos(){
		this.listeners = new HashSet<Listener>();
		novosPedidos = new ArrayList<Pedido>();
	}
	
	@Override
	public void register(Listener listener) {
		this.listeners.add(listener);
	}

	@Override
	public void delete(Listener listener) {
		this.listeners.remove(listener);		
	}

	@Override
	public void notifi() {
		for (Listener listener : listeners) {
			listener.update();
		}		
		novosPedidos.clear();
	}
	
	@Override
	public void novosPedidos(List<Pedido> pedidos) {
		this.novosPedidos.addAll(pedidos);		
	}
	
	@Override
	public List<Pedido> getNovosPedidos(){
		return this.novosPedidos;		
	}

}
