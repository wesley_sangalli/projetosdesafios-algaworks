package br.com.observer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Pedido implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3967567244380114183L;
	
	private String descricao;
	private BigDecimal valor;	
	
	public Pedido(String descricao, BigDecimal valor) {	
		this.descricao = descricao;
		this.valor = valor;		
	}
	
	/**
	 * @return the descricao
	 */
	public String getDescricao() {
		return descricao;
	}
	/**
	 * @param descricao the descricao to set
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	/**
	 * @return the valor
	 */
	public BigDecimal getValor() {
		return valor;
	}
	/**
	 * @param valor the valor to set
	 */
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
