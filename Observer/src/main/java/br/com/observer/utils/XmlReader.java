package br.com.observer.utils;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import br.com.observer.model.Pedido;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class XmlReader {
	@SuppressWarnings("unchecked")
	public List<Pedido> readPedidos(String filePath){
		XStream stream = new XStream(new DomDriver());
		stream.alias("pedidos", List.class);
		stream.alias("pedido", Pedido.class);
		return (List<Pedido>) stream.fromXML(new File(filePath));		
	}
}

