package br.com.observer.listeners;

import java.math.BigDecimal;

import br.com.observer.interfaces.Listener;
import br.com.observer.interfaces.Notifier;
import br.com.observer.model.Pedido;

public class SaleListener implements Listener {
	
	private final Notifier notifier;

	
	public SaleListener(Notifier notifier) {	
		notifier.register(this);
		this.notifier = notifier;		
	}

	@Override
	public BigDecimal update() {
		BigDecimal totalVenda = new BigDecimal(0);
		String message = "-- Nova venda Realizada! Segue a Rela��o: ";		
		for (Pedido pedido  : notifier.getNovosPedidos()) {
			message+= "\r\n"+pedido.getDescricao();			
			totalVenda = totalVenda.add(pedido.getValor());
		}
		
		message+= "\r Total Venda: "+ totalVenda;
		
		System.out.println(message);
		
		return totalVenda;	
	}

	/**
	 * @return the notifier
	 */
	public Notifier getNotifier() {
		return notifier;
	}

}
