package br.com.observer.listeners;

import java.math.BigDecimal;

import br.com.observer.interfaces.Listener;
import br.com.observer.interfaces.Notifier;
import br.com.observer.model.Pedido;

public class TruckListener implements Listener {

	private final Notifier notifier;


	public TruckListener(Notifier notifier) {
		this.notifier = notifier;	
	}

	@Override
	public BigDecimal update() {
		String message = "-- Novos Pedidos recebidos, separar produtos para entrega: ";
		for (Pedido pedido  : notifier.getNovosPedidos()) {
			message+= "\r\n"+pedido.getDescricao();	
		}		
		System.out.println(message);	
		return new BigDecimal(notifier.getNovosPedidos().size());
	}
}
