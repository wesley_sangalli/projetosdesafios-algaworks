package br.com.observer.interfaces;

import java.util.List;

import br.com.observer.model.Pedido;

public interface Notifier {
	public void register(Listener listener);
	public void delete(Listener listener);
	public void notifi();
	
	public void novosPedidos(List<Pedido> pedidos);
	public List<Pedido> getNovosPedidos();
}
