package br.com.observer.interfaces;

import java.math.BigDecimal;

public interface Listener {
	
	public BigDecimal update();

}
