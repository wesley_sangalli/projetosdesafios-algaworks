package br.com.observer;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import br.com.observer.interfaces.Listener;
import br.com.observer.interfaces.Notifier;
import br.com.observer.listeners.SaleListener;
import br.com.observer.listeners.TruckListener;
import br.com.observer.model.Pedido;
import br.com.observer.notifiers.NotifierNovosPedidos;


public class TestPrincipal {

	@Test
	public void testaSomatoria() {
	
		Notifier notifier = new NotifierNovosPedidos();	
		Pedido pedidoSabonete = new Pedido("Sabonete", new BigDecimal(100));
		Pedido pedidoBolacha = new Pedido("Bolacha", new BigDecimal(100));
		Pedido pedidoFarinha = new Pedido("Farinha", new BigDecimal(100));
		
		List<Pedido> pedidos = (List<Pedido>) Arrays.asList(pedidoSabonete, pedidoBolacha, pedidoFarinha);
		Listener listener = new SaleListener(notifier);	
		notifier.novosPedidos(pedidos);
		assertEquals(new BigDecimal(300), listener.update());		
	}
	
	@Test
	public void testaTamanhoLista() {
		
		Notifier notifier = new NotifierNovosPedidos();	
		Pedido pedidoSabonete = new Pedido("Sabonete", new BigDecimal(100));
		Pedido pedidoBolacha = new Pedido("Bolacha", new BigDecimal(100));
		Pedido pedidoFarinha = new Pedido("Farinha", new BigDecimal(100));
		
		List<Pedido> pedidos = (List<Pedido>) Arrays.asList(pedidoSabonete, pedidoBolacha, pedidoFarinha);
		Listener listener = new TruckListener(notifier);	
		notifier.novosPedidos(pedidos);
		assertEquals(new BigDecimal(3), listener.update());		
	}

}
