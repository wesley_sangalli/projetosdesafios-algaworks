package com.inject.bean.impl;

import java.util.Date;
import java.util.GregorianCalendar;

import com.inject.bean.DefinidorTipo;

public class DefinidorTipoImpl implements DefinidorTipo{

	@Override
	public String define(Date data) {
		String tipo = "";
		Date atual = GregorianCalendar.getInstance().getTime();
		
		int mesAtual = atual.getMonth();
		int mesLancamento = data.getMonth();
		if(data.getYear() < atual.getYear()){
			tipo = "Antigo";
		}else if(mesAtual -3 > mesLancamento){
			tipo = "Antigo";
		}else{
			tipo = "lancamento";
		}
		return tipo;
	}

}
