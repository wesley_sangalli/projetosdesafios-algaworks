
package com.inject.bean.impl;

import javax.inject.Inject;

import com.inject.bean.DefinidorTipo;
import com.inject.bean.FilmesService;
import com.inject.model.Filme;
import com.inject.repository.FilmeInfo;

public class FilmesServiceImpl implements FilmesService{
	@Inject FilmeInfo filmeInfo; 
	@Inject DefinidorTipo definidor;
	
	@Override
	public void salvar(Filme filme) {
		String tipoFilme = definidor.define(filme.getDataLancamento());
		filme.setTipo(tipoFilme);
		filmeInfo.salvar(filme);		
	}

}
