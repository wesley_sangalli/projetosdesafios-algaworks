package com.inject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

import com.inject.bean.FilmesService;
import com.inject.model.Filme;
import com.inject.util.cdi.WeldContext;

public class Principal {
	public static void main(String[] Args){
		String nome = JOptionPane.showInputDialog("Nome do filme");
		String genero = JOptionPane.showInputDialog("Genero");		
		String dataLancamento = JOptionPane.showInputDialog("Data de lançamento");	
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyy");		
		Date data;
		try {
			data = format.parse(dataLancamento);
		} catch (ParseException e) {			
			throw new RuntimeException("Erro ao fomatar data", e);
		}
		
		FilmesService service = WeldContext.INSTANCE.getBean(FilmesService.class);
		Filme filme = new Filme(nome, genero, data);
		service.salvar(filme);		
	}
}
