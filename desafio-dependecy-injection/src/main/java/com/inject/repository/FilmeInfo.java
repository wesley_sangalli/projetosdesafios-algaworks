package com.inject.repository;

import com.inject.model.Filme;

public interface FilmeInfo {
	public void salvar(Filme filme);
}
