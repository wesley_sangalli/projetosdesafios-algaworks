package com.inject.repository.xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import com.inject.model.Filme;
import com.inject.repository.FilmeInfo;
import com.thoughtworks.xstream.XStream;

public class XMLFilmeInfo implements FilmeInfo {

	@Override
	public void salvar(Filme filme) {
		XStream stream = new XStream();
		stream.alias("Filme", Filme.class);
		OutputStream out = null;

		try {
			out = new FileOutputStream(filme.getNome() + ".xml");
			stream.toXML(filme, out);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Erro salvando arquivo", e);
		}		
	}

}
