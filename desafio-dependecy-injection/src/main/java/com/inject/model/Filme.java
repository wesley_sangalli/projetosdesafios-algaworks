package com.inject.model;

import java.util.Calendar;
import java.util.Date;

public class Filme {
	
	private String nome;
	private String genero;
	private Date dataLancamento;
	private String tipo;
	
	/**
	 * 
	 * @param nome
	 * @param genero
	 * @param dataLancamento 
	 */
	public Filme(String nome, String genero, Date dataLancamento) {
		this.nome = nome;
		this.genero = genero;
		this.dataLancamento = dataLancamento;		
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getGenero() {
		return genero;
	}
	public void setGenero(String genero) {
		this.genero = genero;
	}
	public Date getDataLancamento() {
		return dataLancamento;
	}
	public void setDataLancamento(Date dataLancamento) {
		this.dataLancamento = dataLancamento;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}	
}
